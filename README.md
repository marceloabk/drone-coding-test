# Drone Trip Planner

## Overview

The Drone Trip Planner is a TypeScript project designed to help plan efficient trips for drones based on their maximum weight capacity and locations with different weights. This project utilizes a first-fit packing algorithm to assign locations to drones in a way that minimizes the number of trips required.

Certainly! Here's a shorter version of the explanation for the README:

## First Fit Packing Algorithm

The First Fit algorithm efficiently assigns locations to drones based on their maximum weight capacity. It works as follows:

1. **Sort Drones and Locations:**

   - Drones are sorted by maximum weight capacity (highest to lowest), and locations are sorted by weight (heaviest to lightest).

2. **Initialize Trips:**

   - Initialize an array to store trips, each representing a set of drones assigned to carry locations.

3. **Assign Locations:**

   - Iterate through locations and for each location, iterate through trips to find a suitable trip.
   - Within each trip, find the first drone with sufficient remaining capacity to carry the location.
   - If found, assign the location to the drone and reduce its remaining capacity; otherwise, create a new trip.

4. **Output:**
   - Produce an array of trips, each containing drones with assigned locations.

### Advantages:

- **Simplicity:** Easy to implement and understand.
- **Efficiency:** Quickly assigns locations to drones without exhaustive search.

### Limitations:

- **Suboptimality:** May not always produce the optimal solution, leading to suboptimal packing.

## Dependencies

- **Node.js:** v20.0.0
- **npm:** v10.0.0

## Getting Started

1. Clone this repository to your local machine.
2. Ensure you have Node.js and npm installed.
3. Run `npm install` to install project dependencies.
4. Place your input file named `Input.txt` inside the `src/input` directory.
5. Run `npm run dev` to compile and execute the project.
6. Once the execution is complete, find the output in the `src/output` directory named `Output.txt`.

## Project Structure

- **src/:** Contains the source code of the project.
  - **input/:** Directory to place input files.
  - **output/:** Directory where output files will be generated.
  - **utils/:** Contains utility functions for file operations and trip planning.

## Scripts

- `npm run dev`: Compiles TypeScript files and executes the project.
- `npm run build`: Compiles TypeScript files and generates JavaScript files.

## Writing, Compiling, and Execution

- **Writing:** This project is written in TypeScript, a superset of JavaScript with static typing.
- **Compilation:** TypeScript files are compiled into JavaScript using the TypeScript compiler (`tsc`).
- **Execution:** The compiled JavaScript files are executed using Node.js.

## IDE

This project can be developed using any text editor or IDE. Some popular choices include:

- Visual Studio Code
- Sublime Text
