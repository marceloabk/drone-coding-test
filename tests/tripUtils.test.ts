import { Drone } from '../src/models/Drone';
import { Location } from '../src/models/Location';
import { Trip } from '../src/models/Trip';
import { firstFitPacking, makeNewTrip } from '../src/utils/tripUtils';

describe('firstFitPacking', () => {
  it('should assign locations to drones using first-fit packing algorithm', () => {
    const drones: Drone[] = [
      { name: 'Drone1', maxWeight: 100 },
      { name: 'Drone2', maxWeight: 150 },
    ];
    const locations: Location[] = [
      { name: 'Location1', weight: 50 },
      { name: 'Location2', weight: 75 },
      { name: 'Location3', weight: 30 },
    ];

    const trips: Trip[] = firstFitPacking(drones, locations);

    expect(trips.length).toBe(1);
    expect(trips[0].tripNumber).toBe(1);
    expect(Object.keys(trips[0].drones).length).toBe(2);
    expect(trips[0].drones['Drone1'].locations.length).toBe(1);
    expect(trips[0].drones['Drone1'].remainingCapacity).toBe(70);
    expect(trips[0].drones['Drone2'].locations.length).toBe(2);
    expect(trips[0].drones['Drone2'].remainingCapacity).toBe(25);
  });
});

describe('makeNewTrip', () => {
  it('should create a new trip with correct initial state', () => {
    const drones = [
      { name: 'Drone1', maxWeight: 100 },
      { name: 'Drone2', maxWeight: 150 },
    ];
    const trip = makeNewTrip(drones, 1);
    expect(trip.tripNumber).toBe(1);
    expect(Object.keys(trip.drones).length).toBe(2);
    expect(trip.drones['Drone1'].locations.length).toBe(0);
    expect(trip.drones['Drone1'].remainingCapacity).toBe(100);
    expect(trip.drones['Drone2'].locations.length).toBe(0);
    expect(trip.drones['Drone2'].remainingCapacity).toBe(150);
  });
});
