export interface Drone {
  name: string;
  maxWeight: number;
}
