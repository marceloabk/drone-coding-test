import { DroneTrip } from './DroneTrip';

export interface Trip {
  tripNumber: number;
  drones: { [key: string]: DroneTrip };
}
