import { Location } from './Location';

export interface DroneTrip {
  locations: Location[];
  remainingCapacity: number;
}
