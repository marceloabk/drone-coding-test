import { Drone } from './models/Drone';
import { Location } from './models/Location';
import {
  readFile,
  writeFile,
  getInputFilePath,
  getOutputFilePath,
} from './utils/fileUtils';
import { firstFitPacking, generateOutput } from './utils/tripUtils';

async function readInput(
  filename: string,
): Promise<{ drones: Drone[]; locations: Location[] }> {
  const data = await readFile(filename);
  const lines = data.split('\n');

  const drones: Drone[] = parseDrones(lines[0]);
  const locations: Location[] = parseLocations(lines.slice(1));

  return { drones, locations };
}

function parseDrones(droneLine: string): Drone[] {
  const droneRegex = /\[([^\]]+)\], \[([^\]]+)\]/g;
  const drones: Drone[] = [];

  let match: RegExpExecArray | null;
  while ((match = droneRegex.exec(droneLine)) !== null) {
    drones.push({
      name: match[1].trim(),
      maxWeight: Number(match[2].trim()),
    });
  }

  return drones;
}

function parseLocations(locationLines: string[]): Location[] {
  const locations: Location[] = [];

  locationLines.forEach((line, index) => {
    const locationRegex = /\[([^\]]+)\], \[([^\]]+)\]/;
    const match = line.match(locationRegex);

    if (!match) {
      throw new Error(`Invalid location format on line ${index + 2}`);
    }

    locations.push({
      name: match[1].trim(),
      weight: Number(match[2].trim()),
    });
  });

  return locations;
}

async function run() {
  const inputFilename = getInputFilePath('Input.txt');
  const outputFilename = getOutputFilePath('Output.txt');

  const { drones, locations } = await readInput(inputFilename);
  const trips = firstFitPacking(drones, locations);
  const output = generateOutput(trips, drones);

  await writeFile(outputFilename, output);
}

run();
