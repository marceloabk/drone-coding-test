import { promises as fs } from 'node:fs';
import path from 'node:path';

export async function readFile(filename: string): Promise<string> {
  return (await fs.readFile(filename, 'utf8')).trim();
}

export async function writeFile(filename: string, data: string): Promise<void> {
  await fs.writeFile(filename, data);
}

export function getInputFilePath(filename: string): string {
  return path.join(__dirname, '..', 'input', filename);
}

export function getOutputFilePath(filename: string): string {
  return path.join(__dirname, '..', 'output', filename);
}
