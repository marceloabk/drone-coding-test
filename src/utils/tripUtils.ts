import { Drone } from '../models/Drone';
import { Location } from '../models/Location';
import { Trip } from '../models/Trip';

export function firstFitPacking(
  drones: Drone[],
  locations: Location[],
): Trip[] {
  drones.sort((a, b) => b.maxWeight - a.maxWeight);
  locations.sort((a, b) => b.weight - a.weight);

  const trips: Trip[] = [makeNewTrip(drones, 1)];

  locations.forEach((location) => {
    let droneFound = false;

    for (const trip of trips) {
      for (const drone of drones) {
        if (trip.drones[drone.name].remainingCapacity >= location.weight) {
          trip.drones[drone.name].locations.push(location);
          trip.drones[drone.name].remainingCapacity -= location.weight;
          droneFound = true;
          break;
        }
      }
      if (droneFound) break;
    }

    if (!droneFound) {
      const newTrip = makeNewTrip(drones, trips.length + 1);
      trips.push(newTrip);
      fitLocationIntoTrip(newTrip, drones, location);
    }
  });

  return trips;
}

export function fitLocationIntoTrip(
  trip: Trip,
  drones: Drone[],
  location: Location,
): void {
  for (const drone of drones) {
    if (trip.drones[drone.name].remainingCapacity >= location.weight) {
      trip.drones[drone.name].locations.push(location);
      trip.drones[drone.name].remainingCapacity -= location.weight;
      break;
    }
  }
}

export function makeNewTrip(drones: Drone[], tripNumber: number): Trip {
  const trip: Trip = { tripNumber, drones: {} };

  drones.forEach((drone) => {
    trip.drones[drone.name] = {
      locations: [],
      remainingCapacity: drone.maxWeight,
    };
  });

  return trip;
}

export function generateOutput(trips: Trip[], drones: Drone[]): string {
  drones.sort((a, b) => a.name.localeCompare(b.name));

  let output = '';

  drones.forEach((drone) => {
    output += `[${drone.name}]\n`;

    trips.forEach((trip) => {
      const droneTrip = trip.drones[drone.name];
      if (droneTrip.locations.length > 0) {
        output += `Trip #${trip.tripNumber}\n`;
        output +=
          droneTrip.locations
            .map((location) => `[${location.name}]`)
            .join(', ') + '\n';
      }
    });

    output += '\n';
  });

  return output;
}
